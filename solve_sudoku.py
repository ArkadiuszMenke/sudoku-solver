#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Created By  : Arkadiusz Menke
# Created Date: Friday 18th June 14:44:27 BST 2021
#
"""This is a sudoku solver that uses recursion and backtracking to identify possible solutions if they exist. It uses
all sudoku rules on a 9x9 grid"""

from time import sleep
from datetime import timedelta, datetime

final = []
timed_out = False

def display_board(board):
    """
    Displays the board which is passed to it

    :param board: a 2d array that stores the sudoku board that is being/has been solved
    """
    output_string = ""
    count = 0
    print("===========================")
    for row in range(len(board)):
        print(board[row])
    print("===========================")
    return True



def check_valid(array, value):
    """
    Checks if the value passed through is in the array passed through and if so, the value is not valid otherwise
    it is.

    :param array - an array representing a row, column or square of a sudoku board that is to be checked against the value
    :param value - a numerical value that is to be checked before being added to the sudoku board

    :return: returns if an array has the value in it or not
    """
    if value in array:
        # print("clash")
        return False
    else:
        return True


def make_square(rows, cols, board):
    """
    Makes an array depicting a square in a sudoku board and is to be used in Check_Valid(). To find squares the 9x9
    grid becomes a 3x3 grid meaning rows and columns are used to find exact placements.

    :param rows: the row in which the square sits on the board (the 9x9 grid becomes a 3x3 grid when looking at squares only)
    :param cols: the column in which the square sits on the board
    :param board: a 2d array that stores the sudoku board that is being/has been solved

    :return returns a newly made square
    """
    square = []
    for i in rows:
        for j in cols:
            square.append(board[i][j])
    return square


def solve_cell(row, col, value, board):
    """
    Validates the value that is to be inputted against the sudoku rules. This means the value cannot appear in
    the same row, column or square that the field sits in.

    :param row: the row in which the current field we want to check sits
    :param col: the column in which the current field we want to check sits
    :param value: the number which we want to input into a specific field
    :param board: a 2d array that stores the sudoku board that is being/has been solved

    :return; returns if value can go in the specified cell
    """

    # Check if value can be entered in the fields row
    if check_valid(board[row], value):

        # Make an array for board columns
        column_values = []
        for i in range(len(board)):
            column_values.append(board[i][col])
        # Check if value can be entered in the fields column
        if check_valid(column_values, value):

            sq_row = []
            sq_col = []

            # Using a 3x3 grid, define where the current square sits
            # Get square row
            if row in [0, 1, 2]:
                sq_row = [0, 1, 2]
            elif row in [3, 4, 5]:
                sq_row = [3, 4, 5]
            elif row in [6, 7, 8]:
                sq_row = [6, 7, 8]

            # Get square column
            if col in [0, 1, 2]:
                sq_col = [0, 1, 2]
            elif col in [3, 4, 5]:
                sq_col = [3, 4, 5]
            elif col in [6, 7, 8]:
                sq_col = [6, 7, 8]

            # Check if value can be entered in the fields square
            if check_valid(make_square(sq_row, sq_col, board), value):
                # Return true as value is in a valid position
                return True
    else:
        # Return false if value is in an invalid position
        return False

def check_input(board):
    """
    Checks if board breaches sudoku rules

    :param board: a 2d array that stores the sudoku board that is to be solved
    :return: Returns if board is valid and does not breach sudoku rules
    """
    for row in range(len(board)):
        for col in range(len(board[row])):
            if board[row][col] != 0:
                inp_value = board[row][col]
                board[row][col] = 0
                if not solve_cell(row, col, inp_value, board):
                    return False
                board[row][col] = inp_value
    return True


def solve(board, timeout):
    """
    Solves the board passed through to it if possible. Uses recursion and backtracking to find possible solutions to
    the sudoku puzzle.

    :param board - a 2d array that stores the sudoku board that is being/has been solved
    :param board - a datetime variable that stores the timeout of when the function should terminate if working too long
    """
    global final, timed_out

    # Check if current time is past the timeout
    if datetime.utcnow() > timeout:
        timed_out = True
        return False
    else:
        # Checks each board row
        for i in range(9):
            # Checks each board column
            for j in range(9):
                # Makes sure that the field is empty before changing it
                if board[i][j] == 0:
                    # Goes through the numbers 1 - 9 to find a possible match
                    for num in range(1, 10):
                        # Checks validity of a number
                        if solve_cell(i, j, num, board):
                            # If valid it changes that field
                            board[i][j] = num
                            # Recursively calls again to try and solve the next fields
                            solve(board, timeout)
                            # Reaching this point means that it could not find a a way to solve the puzzle using this
                            # value so it backtracks and resets it to 0
                            board[i][j] = 0
                    return

        # Arranges and appends the final solved board to a global variable to be used by the calling function
        temp = []
        final = []
        for row in range(len(board)):
            for col in range(len(board[row])):
                temp.append(board[row][col])
            final.append(temp)
            temp = []

def solve_manager(board):
    """
    Calls the solve function to solve the board and returns the final output if possible. If solve function takes too
    long the timeout is triggered and the function returns None

    :param board: a 2d array that stores the sudoku board that is being/has been solved
    :return: returns the final solved board if possible, otherwise it returns None
    """
    global timed_out

    # Sets up timeout for when function needs to terminate
    timeout = datetime.utcnow() + timedelta(seconds=3)
    timed_out = False

    # Attempts to solve the board
    solve(board, timeout)

    # If timeout flag is tripped, return None
    if(timed_out):
        return None

    # Return the solved board if no timeout triggered
    return final
