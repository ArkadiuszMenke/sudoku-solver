#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Created By  : Arkadiusz Menke
# Created Date: Friday 18th June 14:44:27 BST 2021
#
"""This is the main function that uses pygame to visualise a sudoku solver. It renders all graphics and calls
solve_sudoku when trying to find a solution for a puzzle entered by the user"""

import solve_sudoku
import pygame
import sys
import tkinter_manager


class Square:
    """
    A class used to represent a visual square on the sudoku board

    Attributes
    :param number: integer showing the current number in the square
    :param selected: boolean determining if the current square is selected by the user or not
    :param colour: determines the colour of the square

    Methods
    make_rectangle(x, y, length, width): makes a new pygame rect and returns it

    """
    def __init__(self, number, selected, colour):
        """
        :param number: integer showing the current number in the square
        :param selected: boolean determining if the current square is selected by the user or not
        :param colour: determines the colour of the square
        """
        self.number = number
        self.selected = selected
        self.colour = colour

    def make_rectangle(self, x, y, length, width):
        """
        Makes a rectangle out of the x and y coordinates and the given length and width parameters

        :param x: an x coordinate to render the rectangle on
        :param y: a y coordinate to render the rectangle on
        :param length: the length of the rectangle being rendered
        :param width: the width of the rectangle being rendered

        :return: returns the newly made rectangle
        """
        new_rect = pygame.Rect(x, y, length, width)
        return new_rect

class Button:
    """
    A class used to represent visual buttons

    :param text: string determining the button text on screen
    :param selected: boolean determining if the current button is selected(pressed)
    :param colour: determines the colour of the button
    """
    def __init__(self, text, selected, colour):
        """
        :param text: string determining the button text on screen
        :param selected: boolean determining if the current button is selected(pressed)
        :param colour: determines the colour of the button
        """
        self.text = text
        self.selected = selected
        self.colour = colour


# Initialises pygame
pygame.init()

# Sets up core variables to use with the pygame module
clock = pygame.time.Clock()
screen = pygame.display.set_mode([600, 600])
base_font = pygame.font.Font(None, 32)

# Sets allowed inputs for the sudoku (numbers between 1-9)
allowed_inputs = [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5,
                  pygame.K_6, pygame.K_7, pygame.K_8, pygame.K_9, pygame.K_0]
squares = []
sub_squares = []

# Variables to store the position of the selected square
selected_square_x = 0
selected_square_y = 0

# Sets the name of the window
pygame.display.set_caption('Sudoku Solver')

# Sets the icon of the window
Icon = pygame.image.load('sudoku_icon.png')
pygame.display.set_icon(Icon)

# Sets the colours used when selecting squares
colour_active = pygame.Color("lightskyblue3")
colour_passive = pygame.Color("gray15")


# Creates a 2d array that stores tuples in the form ( square object , pygame rectangle )
cc = 0
temp_grid = []
x_pos = 50
y_pos = 0
for i in range(9):
    for j in range(9):
        # Make new square and rectangle
        new_square = Square(0, False, colour_passive)
        new_rect = new_square.make_rectangle(x_pos, y_pos, 50, 50)
        square_tup = (new_square, new_rect)
        temp_grid.append(square_tup)
        if j == 8:
            squares.append(temp_grid)
            temp_grid = []
            count = 0
        else:
            x_pos += 50
    x_pos = 50
    y_pos += 50

# Creates visual representation of sudoku squares which are a 3x3 subset of the board
sub_x = 50
sub_y = 0
for x in range(3):
    for y in range(3):
        board_square = pygame.Rect(sub_x, sub_y, 150, 150)
        sub_squares.append(board_square)
        sub_x += 150
    sub_x = 50
    sub_y += 150

# Creates solve button
solve_btn = Button("Solve", False, colour_passive)
solve_rectangle = pygame.Rect(50, 450, 140, 32)

# Creates clear button
clear_btn = Button("Clear", False, colour_passive)
clear_rectangle = pygame.Rect(200, 450, 140, 32)


# Establishes the game loop
while True:

    # Checks pygame events
    for event in pygame.event.get():
        # Quit game
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        # Identify if user pressed on a valid square
        if event.type == pygame.MOUSEBUTTONDOWN:

            # If user pressed solve button
            if solve_rectangle.collidepoint(event.pos):
                # Initialise storage variables
                solve_board = []
                temp = []
                final_board = []

                # Try solving the current board
                try:
                    # Set up board to be solved
                    for row in range(len(squares)):
                        for col in range(len(squares[row])):
                            temp.append(squares[row][col][0].number)
                        solve_board.append(temp)
                        temp = []

                    # If initial values pass sudoku rules
                    if solve_sudoku.check_input(solve_board):

                        # Send set up board to the sudoku solver to and return the solved board
                        final_board = solve_sudoku.solve_manager(solve_board)

                        # Check if board was not solvable
                        if not final_board:
                            tkinter_manager.popup("Alert", "Board could not be solved")
                        else:
                            # Iterate through the solution and append it to the visual sudoku board
                            for x in range(len(final_board)):
                                for y in range(len(final_board[x])):
                                    squares[x][y][0].number = final_board[x][y]
                        # If values do not pass sudoku rules
                    else:
                        # Display alert to tell user to change inputs
                        tkinter_manager.popup("Input Error", "Board entered breaches sudoku rules")

                # If board cannot be solved
                except:
                    tkinter_manager.popup("Alert", "Board cannot be solved")


            # If user pressed clear button
            elif clear_rectangle.collidepoint(event.pos):
                # Iterate through the visual board and clear everything to 0
                for x in range(len(squares)):
                    for y in range(len(squares[x])):
                        squares[x][y][0].number = 0

            else:
                # Iterate through the board
                for r in range(len(squares)):
                    for c in range(len(squares[r])):
                        # Check if a specific square was pressed
                        if squares[r][c][1].collidepoint(event.pos):
                            # Selects the square and updates temporary variables
                            squares[r][c][0].selected = True
                            selected_square_x = r
                            selected_square_y = c
                        else:
                            if squares[r][c][0].selected:
                                squares[r][c][0].selected = False

        # Identify user input and, if valid, update the selected square
        if event.type == pygame.KEYDOWN:
            # Checks if a square is selected using variables which store the last square x and y numbers
            if squares[selected_square_x][selected_square_y][0].selected:
                if event.key == pygame.K_BACKSPACE:
                    squares[selected_square_x][selected_square_y][0].number = 0
                elif event.key in allowed_inputs:
                    squares[selected_square_x][selected_square_y][0].number = int(event.unicode)

    # Fill screen with white
    screen.fill((255, 255, 255))

    # Render board sub squares
    for sub in range(len(sub_squares)):
        pygame.draw.rect(screen, colour_passive, sub_squares[sub], 4)
        sub_surface = base_font.render("", True, (0, 0, 0))
        screen.blit(sub_surface, (sub_squares[sub].x, sub_squares[sub].y))
        sub_squares[sub].w = max(150, sub_surface.get_width() + 10)

    # Render all sudoku squares and update colour coding for selected squares
    for row in range(len(squares)):
        for column in range(len(squares[row])):
            # Colour code boxes
            if squares[row][column][0].selected:
                squares[row][column][0].colour = colour_active
            else:
                squares[row][column][0].colour = colour_passive

            # Render all squares
            pygame.draw.rect(screen, squares[row][column][0].colour, squares[row][column][1], 2)
            text_surface = base_font.render(str(squares[row][column][0].number), True, (0, 0, 0))
            screen.blit(text_surface, (squares[row][column][1].x + 5, squares[row][column][1].y + 5))
            squares[row][column][1].w = max(50, text_surface.get_width() + 10)

    # Render solve button
    pygame.draw.rect(screen, solve_btn.colour, solve_rectangle, 2)
    solve_surface = base_font.render(str(solve_btn.text), True, (0, 0, 0))
    screen.blit(solve_surface, (solve_rectangle.x + 5, solve_rectangle.y + 5))
    solve_rectangle.w = max(150, solve_surface.get_width() + 10)

    # Render clear button
    pygame.draw.rect(screen, clear_btn.colour, clear_rectangle, 2)
    clear_surface = base_font.render(str(clear_btn.text), True, (0, 0, 0))
    screen.blit(clear_surface, (clear_rectangle.x + 5, clear_rectangle.y + 5))
    clear_rectangle.w = max(150, clear_surface.get_width() + 10)

    pygame.display.flip()
    clock.tick(60)