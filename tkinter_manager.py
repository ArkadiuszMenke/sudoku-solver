#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Created By  : Arkadiusz Menke
# Created Date: Friday 25th June 13:01:38 BST 2021
#

import tkinter as tk
from tkinter import messagebox


def popup(title, message):
    """
    Uses the tkinter library to display a popup with a set title and message

    title - a string defining the title of the popup
    message - a string defining the message of the popup
    """
    tk.Tk().withdraw()
    name = messagebox.showinfo(title, message)
