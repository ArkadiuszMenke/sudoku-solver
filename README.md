# Sudoku Solver

This is a small personal project where I implement a sudoku puzzle solver where the user can input a puzzle and have it solved in seconds. Validation exists too to help ease the use of the system.

The sudoku solver itself uses *recursion* and *backtracking* to derive an answer from the given input board. This is called in the main function which handles all the pygame related visuals, showing an input board with usable buttons for solving and clearing the board. Tkinter is also used to implement popups if any errors do arise such as when the board cannot be solved due to a timeout.

## Usage
The program itself has an integrated user interface allowing for simple usage of the sudoku solver. All that you need to do to solve a sudoku puzzle is input the numbers into the given grid and press solve. This will be solved and displayed in the same grid if the input was a valid puzzle.

To use the sudoku solver a call to the solve_manager function in the solve_sudoku.py file is made like listed below:
```python
# Send set up board to the sudoku solver to and return the solved board
final_board = solve_sudoku.solve_manager(solve_board)
```
where final_board is a vessel for the solved board (or None if the board solver timed out).

## Dependencies
- Pygame v2.0.1

## Author
Arkadiusz Menke


